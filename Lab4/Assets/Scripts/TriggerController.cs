﻿using UnityEngine;

public class TriggerController : MonoBehaviour
{
    private Transform objTransform;
    private bool isInTrigger = false;


    // Start is called before the first frame update
    void Start()
    {
        objTransform = GetComponent<Transform>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Collider>().CompareTag("Player"))
        {
            isInTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Collider>().CompareTag("Player"))
        {
            isInTrigger = false;
        }
    }

    void Update()
    {
        if (isInTrigger)
            objTransform.Rotate(new Vector3(1, 0, 1), 1f);
    }
}