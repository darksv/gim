﻿using UnityEngine;

public class NewBehaviourScript1 : MonoBehaviour
{
    private Animator animator;
    
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        animator.SetBool("DoWalk", Input.GetKey(KeyCode.Alpha1));
        animator.SetBool("DoTurn", Input.GetKey(KeyCode.Alpha2));
        animator.SetBool("DoRun", Input.GetKey(KeyCode.Alpha3));
    }
}
