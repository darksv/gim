﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.W))
            animator.SetBool("GoForward", false);
        else if (Input.GetKeyDown(KeyCode.W))
            animator.SetBool("GoForward", true);
        
        if (Input.GetKeyUp(KeyCode.D))
            animator.SetBool("GoRight", false);
        else if (Input.GetKeyDown(KeyCode.D))
            animator.SetBool("GoRight", true);
    }
}