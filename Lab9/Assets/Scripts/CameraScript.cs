﻿using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private Vector3 offset;
    public GameObject target;

    void Start()
    {
        offset = target.transform.position - transform.position;
    }

    void LateUpdate()
    {
         transform.position = target.transform.position - offset;
    }
}
