﻿using UnityEngine;

public class PyramidGenerator : MonoBehaviour
{
    public GameObject item;
    public GameObject origin;

    void Start()
    {
        for (int k = 0; k < 11; k++)
        {
            var n = k;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Instantiate(
                        item,
                        origin.transform.position + new Vector3(
                            0.3f * (11 - k) + i * 0.6f,
                            11.0f - k + 0.5f,
                            0.3f * (11 - k) + j * 0.6f),
                        Quaternion.identity
                    );
                }
            }
        }
    }
}