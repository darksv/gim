﻿using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour
{
    public GameObject projectile;
    public Button fireButton;
    public Slider forceSlider;
    public Slider angleSlider;
    public Canvas uiCanvas;

    void Start()
    {
        fireButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            Destroy(uiCanvas);
            uiCanvas.gameObject.SetActive(false);
            OnClick();
        });
    }

    void OnClick()
    {
        var rigidBody = projectile.GetComponent<Rigidbody>();
        rigidBody.useGravity = true;

        var value = forceSlider.value;
        var angle = Mathf.Deg2Rad * angleSlider.value;
        var y = value * Mathf.Sin(angle);
        var z = value * Mathf.Cos(angle);

        var constantForce = projectile.GetComponent<ConstantForce>();
        constantForce.force = new Vector3(0, y, z);
    }
}
