﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterfallController : MonoBehaviour
{
    public GameObject waterfall;
    private ParticleSystem component;
    private bool isPlaying;

    void Start()
    {
        component = waterfall.GetComponent<ParticleSystem>();
        if (isPlaying)
            component.Play();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isPlaying)
            {
                component.Stop();
                isPlaying = false;
            }
            else
            {
                component.Play();
                isPlaying = true;
            }
        }
    }
}
