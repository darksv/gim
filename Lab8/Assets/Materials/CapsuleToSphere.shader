﻿Shader "Custom/NewSurfaceShader" {
	SubShader{
		Pass {
			CGPROGRAM

			#pragma vertex vert 
			#pragma fragment frag

			float4 vert(float4 vertexPos : POSITION) : SV_POSITION
			{
				float4 newPos = vertexPos.y > 0.5 
					? float4(vertexPos.x, vertexPos.y - 1, vertexPos.zw) 
					: vertexPos;
				return UnityObjectToClipPos(newPos + float4(0, 0.5, 0, 0));
			}

			float4 frag(void) : COLOR
			{
				return float4(1.0, 0.0, 0.0, 1.0);
			}

			ENDCG
		}
	}
}