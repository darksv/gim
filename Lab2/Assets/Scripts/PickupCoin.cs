﻿using UnityEngine;

public class PickupCoin : MonoBehaviour
{
    public int value = 0;

    private ScoreScript scoreScript;

    void Awake()
    {
        scoreScript = GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreScript>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            scoreScript.score += value;
        }
    }
}