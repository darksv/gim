﻿using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public Transform[] coinSpawns;
    public GameObject[] coins;

    void Start()
    {
        Spawn();
    }

    private void Spawn()
    {
        for (int i = 0; i < coinSpawns.Length; i++)
        {
            int coinFlip = Random.Range(0, 2);
            if (coinFlip > 0)
            {
                var coin = coins[Random.Range(0, coins.Length)];
                Instantiate(coin, coinSpawns[i].position, Quaternion.identity);
            }
        }
    }
}