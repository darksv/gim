﻿using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    public int maxPlatforms = 20;
    public GameObject platform;
    public float horizontalMin = 7.5f;
    public float horizontalMax = 14f;
    public float verticalMin = -6f;
    public float verticalMax = 6f;
    private Vector2 originPosition;

    void Start()
    {
        originPosition = transform.position;
        Spawn();
    }

    private void Spawn()
    {
        for (int i = 0; i < maxPlatforms; i++)
        {
            var randomPosition = originPosition + new Vector2(
                                     Random.Range(horizontalMin, horizontalMax),
                                     Random.Range(verticalMin, verticalMax)
                                 );
            Instantiate(platform, randomPosition, Quaternion.identity);
            originPosition = randomPosition;
        }
    }
}