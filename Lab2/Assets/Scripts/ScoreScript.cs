﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public int score = 0;

    public Text scoreText;
    public Text timeText;
    public Text wonText;

    private float time = 30.0f;
    private bool isWon = false;

    void Start()
    {
        score = 0;
        time = 30.0f;
    }

    void Update()
    {
        if (isWon)
        {
            wonText.text = "You won!";
            return;
        }

        time -= Time.deltaTime;
        if (time <= 0.0f)
        {
            time = 0.0f;
            Application.LoadLevel(Application.loadedLevel);
        }

        if (score >= 30)
        {
            isWon = true;
        }

        scoreText.text = string.Format("Score: {0}", score);
        timeText.text = string.Format("Time: 0:{0}", Mathf.CeilToInt(time).ToString().PadLeft(2, '0'));
    }
}
