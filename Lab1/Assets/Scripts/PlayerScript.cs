﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public float speed;
    private int score = 0;
    private bool isGameOver = false;

    public Text scoreText;
    public Text winText;

    void Start()
    {
        UpdateScore();
    }

    void Update()
    {
        var component = GetComponent<Rigidbody>();
        if (isGameOver)
        {
            component.velocity = Vector3.zero;
            
            return;
        }

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        component.AddForce(movement * speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup1"))
        {
            score += 1;
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.CompareTag("Pickup2"))
        {
            score += 4;
            other.gameObject.SetActive(false);
        }

        UpdateScore();

        if (score >= 9)
        {
            winText.text = "You won!!";
            isGameOver = true;
        }
    }

    private void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }
}